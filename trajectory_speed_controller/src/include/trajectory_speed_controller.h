#ifndef trajectory_speed_controller
#define trajectory_speed_controller

/*!*************************************************************************************
 *  \class     trajectory_speed_controller
 *
 *  \brief     Trajectory Speed Controller
 *
 *  \details   This class is in charge of control a trajectory using PID class.
 *             Sends as Output Speed References.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>


#include "droneMsgsROS/dronePositionRefCommandStamped.h"

#include "droneMsgsROS/dronePose.h"

#include "droneMsgsROS/droneYawRefCommand.h"

#include "droneMsgsROS/droneSpeeds.h"

#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"

#include "xmlfilereader.h"

#include "math.h"


class TrajectorySpeedController
{
public:

  void setUp();

  void start();

  void stop();

  void Initialize();

  void straigh();
  void turn();
  void setReference(droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_ref);
  void setFeedback(droneMsgsROS::dronePose last_Pose, droneMsgsROS::droneSpeeds last_Speed);
  void calculate();
  float max(float d1,float d2);

  void getOutput(float *dx_out, float *dy_out, float *dz_out, bool *trj_end_out);

  //! Read Config
  bool readConfigs(std::string configFile);

  //! Constructor. \details Same arguments as the ros::init function.
  TrajectorySpeedController();

  //! Destructor.
  ~TrajectorySpeedController();

private:



  //! ROS NodeHandler used to manage the ROS communication
  ros::NodeHandle nIn;

  //! Configuration file variable
  int idDrone;               // Id Drone integer (number of the drone)
  std::string trjspdconfigFile;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name


  //! ROS publisher
  ros::Publisher pub_posref;


  // Position
  float x;
  float y;
  float z;

  // Speed Commanded
  float dx;
  float dy;
  float dz;

  // Constants
  float v_maxxy;
  float v_maxz;
  float error_max;
  float radius;


  float distance;
  float distance_real;



  // Trajectory
  droneMsgsROS::dronePositionTrajectoryRefCommand trajectory;
  int initial_checkpoint;
  droneMsgsROS::dronePositionRefCommand last_point;
  droneMsgsROS::dronePositionRefCommand actual_point;
  droneMsgsROS::dronePositionRefCommand next_point;
  float number_point;
  int iterator;

  bool calculated;
  bool straigh_end;
  bool turn_end;
  bool last_waypoint;
  bool start_done;

  float next_yaw;
  float actual_yaw;


  float distx;
  float disty;
  float distxy;
  float distz;
  float next_distx;
  float next_disty;
  float next_distxy;
  float next_distz;

  float t_turn;
  float t;
  float time;

  float vx;
  float vy;
  float vxy;
  float vz;
  float next_vx;
  float next_vy;
  float next_vxy;
  float next_vz;

  bool Start_Stop_Trajectory_Controller;

};
#endif 
