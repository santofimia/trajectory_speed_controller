#include "trajectory_speed_controller.h"

//Constructor
TrajectorySpeedController::TrajectorySpeedController()
{
    std::cout << "Constructor: TrajectorySpeedController" << std::endl;

}

//Destructor
TrajectorySpeedController::~TrajectorySpeedController() {}


void TrajectorySpeedController::Initialize()
{
    calculated = false;
}


bool TrajectorySpeedController::readConfigs(std::string configFile)
{

    try
    {

    XMLFileReader my_xml_reader(configFile);

    /*********************************  Constant Needed ************************************************/

    v_maxxy = my_xml_reader.readDoubleValue("Trajectory_Speed_Controller:Constants:Vmaxxy");
    v_maxz = my_xml_reader.readDoubleValue("Trajectory_Speed_Controller:Constants:Vmaxz");
    error_max = my_xml_reader.readDoubleValue("Trajectory_Speed_Controller:Constants:error_max");
    radius = my_xml_reader.readDoubleValue("Trajectory_Speed_Controller:Constants:radius");

    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }

    return true;

}

void TrajectorySpeedController::setUp()
{
    Initialize();

    ros::param::get("~stackPath", stackPath);
    if ( stackPath.length() == 0)
    {
        stackPath = "$(env AEROSTACK_STACK)";
    }
    ros::param::get("~droneId", idDrone);
    ros::param::get("~trjspd_config_file", trjspdconfigFile);
    if ( trjspdconfigFile.length() == 0)
    {
        trjspdconfigFile="trajectory_speed_controller.xml";
    }

    bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+cvg_int_to_string(idDrone)+"/"+trjspdconfigFile);

    if(!readConfigsBool)
    {
        std::cout << "Error init"<< std::endl;
        return;
    }

    std::cout << "Constructor: TrajectorySpeedController...Exit" << std::endl;


}

void TrajectorySpeedController::start()
{

//    // Publisher
    pub_posref = (this->nIn).advertise<droneMsgsROS::dronePositionRefCommandStamped>("dronePositionRefs", 1,this);

    iterator = 0;
    straigh_end = false;
    calculated = false;
    last_waypoint = false;
    start_done = false;
    turn_end = true;

}

void TrajectorySpeedController::stop()
{
}

void TrajectorySpeedController::straigh()
{

    distx = actual_point.x-x;
    disty = actual_point.y-y;
    distxy = sqrt(pow(actual_point.x-x,2.0)+pow(actual_point.y-y,2.0));
    distz = actual_point.z-z;

    if ((distxy/v_maxxy) >= abs(distz/v_maxz)) t = (distxy/v_maxxy);
    else t = abs(distz/v_maxz);
    if (t != 0.0){
        dx = distx/t;
        dy = disty/t;
        dz = distz/t;
    }else {
        dz = 0.0;
        dx = 0.0;
        dy = 0.0;
    }

}

void TrajectorySpeedController::turn()
{
    if (time == 0.0){
        time = ros::Time::now().toSec();
    }

    if (((ros::Time::now().toSec()-time) <= t_turn)&&(t_turn > 0.0)){
        dx = vx*(1.0-(ros::Time::now().toSec()-time)/t_turn) + next_vx*(ros::Time::now().toSec()-time)/t_turn;
        dy = vy*(1.0-(ros::Time::now().toSec()-time)/t_turn) + next_vy*(ros::Time::now().toSec()-time)/t_turn;
        dz = vz*(1.0-(ros::Time::now().toSec()-time)/t_turn) + next_vz*(ros::Time::now().toSec()-time)/t_turn;

    } else{
        time = 0.0;
        iterator++;
        calculated = false;
        turn_end = true;
        straigh_end = false;
        if (iterator >= number_point){
            if (trajectory.is_periodic) iterator = 0;

        }
    }
}



void TrajectorySpeedController::setReference(droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_ref){
    //! Check if there are points on trajectory  checking size??
    if (trajectory_ref.droneTrajectory.size()>0){
        trajectory = trajectory_ref;
        number_point = trajectory_ref.droneTrajectory.size();
        initial_checkpoint = trajectory.initial_checkpoint;
        if (initial_checkpoint != 0){
            trajectory.droneTrajectory.clear();
            std::vector<droneMsgsROS::dronePositionRefCommand>::const_iterator it_mod = (trajectory_ref.droneTrajectory).begin();
            for (std::vector<droneMsgsROS::dronePositionRefCommand>::const_iterator it = (trajectory_ref.droneTrajectory).begin();
                 it != (trajectory_ref.droneTrajectory).end();
                 ++it) {

                if (it + initial_checkpoint - (trajectory_ref.droneTrajectory).begin() >= number_point && !start_done) {

                    it_mod = (trajectory_ref.droneTrajectory).begin();
                    start_done = true;

                } else if (it + initial_checkpoint - (trajectory_ref.droneTrajectory).begin() >= number_point&& start_done){

                    it_mod++;

                } else {

                    it_mod = it + initial_checkpoint;

                }

                droneMsgsROS::dronePositionRefCommand next_waypoint;
                next_waypoint.x = it_mod->x;
                next_waypoint.y = it_mod->y;
                next_waypoint.z = it_mod->z;
                trajectory.droneTrajectory.push_back(next_waypoint);
                std::cout<<next_waypoint<<std::endl;
            }

            trajectory.initial_checkpoint = 0;
        }

    }
    calculated = false;
    iterator = 0;

}

void TrajectorySpeedController::setFeedback(droneMsgsROS::dronePose last_Pose, droneMsgsROS::droneSpeeds last_Speed){

    x = last_Pose.x;
    y = last_Pose.y;
    z = last_Pose.z;

}

void TrajectorySpeedController::getOutput(float *dx_out, float *dy_out, float *dz_out, bool *trj_end_out){

    if (iterator < number_point){

        if (!calculated ){
            calculate();
        }

        distance_real = max (sqrt(pow(actual_point.x-x,2.0)+pow(actual_point.y-y,2.0)),abs(actual_point.z-z));
        if (distance_real >= distance){
            straigh();
        }
        else{
            straigh_end = true;
        }

        if (straigh_end){
            turn_end = false;
            turn();
        }

        *dx_out = dx;
        *dy_out = dy;
        *dz_out = dz;
        *trj_end_out = false;
    }
    else{

        *dx_out = 0.0;
        *dy_out = 0.0;
        *dz_out = 0.0;
        *trj_end_out = true;

    }

}

void TrajectorySpeedController::calculate(){

    if (iterator == 0){
        last_point.x = x;
        last_point.y = y;
        last_point.z = z;
    }else last_point = actual_point;

    actual_point = trajectory.droneTrajectory[iterator];

    droneMsgsROS::dronePositionRefCommandStamped  drone_position_reference;
    drone_position_reference.position_command.x = actual_point.x;
    drone_position_reference.position_command.y = actual_point.y;
    drone_position_reference.position_command.z = actual_point.z;
    pub_posref.publish(drone_position_reference);

    if ((actual_point.x-last_point.x) != 0.0){
            actual_yaw = + atan((actual_point.y-last_point.y)/(actual_point.x-last_point.x));
            if ((actual_point.x-last_point.x) < 0.0) {
                actual_yaw -= M_PI;
            }
    }else{
        if ((actual_point.y-last_point.y) > 0.0) actual_yaw = +M_PI/2;
        else if ((actual_point.y-last_point.y) < 0.0) actual_yaw = -M_PI/2;
        else actual_yaw = 0.0;
    }
    if (actual_yaw > M_PI){
        actual_yaw -= 2.0 * M_PI;
    }
    else if(actual_yaw < -M_PI){
        actual_yaw += 2.0 * M_PI;
    }


    if (iterator+1 >= number_point) {
        if (trajectory.is_periodic){
            next_point = trajectory.droneTrajectory[0];
        }
        else{
            last_waypoint = true;
            next_point = actual_point;
        }
    }
    else next_point = trajectory.droneTrajectory[iterator+1];

    distx = actual_point.x-last_point.x;
    disty = actual_point.y-last_point.y;
    distz = actual_point.z-last_point.z;

    distxy = sqrt(pow(distx,2.0)+pow(disty,2.0));

    next_distx = next_point.x-actual_point.x;
    next_disty = next_point.y-actual_point.y;
    next_distz = next_point.z-actual_point.z;

    next_distxy = sqrt(pow(next_distx,2.0)+pow(next_disty,2.0));


    if ((next_point.x == actual_point.x)&&(next_point.y == actual_point.y)&&(next_point.z == actual_point.z)) next_yaw = actual_yaw;
    else {
        if ((next_point.x-actual_point.x) != 0.0){
                next_yaw = + atan((next_point.y-actual_point.y)/(next_point.x-actual_point.x));
                if ((next_point.x-actual_point.x) < 0.0) next_yaw -= M_PI;

        }else{
            if ((next_point.y-actual_point.y) > 0.0) next_yaw = +M_PI/2;
            else if ((next_point.y-actual_point.y) < 0.0) next_yaw = -M_PI/2;
            else next_yaw = 0.0;
        }
        if (next_yaw > M_PI){
            next_yaw -= 2.0 * M_PI;
        }
        else if(next_yaw < -M_PI){
            next_yaw += 2.0 * M_PI;
        }
    }

    if (actual_yaw - next_yaw < M_PI) next_yaw -= 2*M_PI;
    if (actual_yaw - next_yaw > M_PI) next_yaw += 2*M_PI;


    if ((distxy/v_maxxy) >= abs(distz/v_maxz)) t = (distxy/v_maxxy);
    else t = abs(distz/v_maxz);
    if (t != 0.0){
        vx = distx/t;
        vy = disty/t;
        vxy = distxy/t;
        vz = distz/t;
    }else {
        vz = 0.0;
        vx = 0.0;
        vy = 0.0;
        vxy = 0.0;
    }

    if ((next_distxy/v_maxxy) >= abs(next_distz/v_maxz)){
        if (next_distxy != 0.0) {
            next_vz = next_distz * v_maxxy / next_distxy;
            next_vx = next_distx * v_maxxy / next_distxy;
            next_vy = next_disty * v_maxxy / next_distxy;
            next_vxy = next_distxy * v_maxxy / next_distxy;
        }
        else {
            next_vz = 0.0;
            next_vx = 0.0;
            next_vy = 0.0;
            next_vxy = 0.0;
        }


    }
    else{
        if (next_distz != 0.0) {
            next_vx = next_distx * v_maxz / abs(next_distz);
            next_vy = next_disty * v_maxz / abs(next_distz);
            next_vxy = next_distxy * v_maxz / abs(next_distz);
            next_vz = v_maxz;
        }
        else {
            next_vz = 0.0;
            next_vx = 0.0;
            next_vy = 0.0;
            next_vxy = 0.0;
        }

    }
    if ((abs(vxy)+abs(next_vxy)) != 0.0){
        t_turn = radius*abs(next_yaw - actual_yaw)/((abs(vxy)+abs(next_vxy))/2.0);
    }else t_turn = 0.0;


    if (last_waypoint) distance = radius + error_max;
    else {
        distance = (radius+error_max)*cos((next_yaw - actual_yaw)/2.0);
//        if (distance < 1.0) radius = 1.0;
    }

    if ((distance > distxy)&&(t != 0.0)){
        t_turn = t_turn*distxy/distance;
        distance = distxy;
    }
    if (distance < 0.50){
        t_turn = t_turn *0.50/distance;
        distance = 0.50;
    }


    calculated = true;
    straigh_end = false;

}


float TrajectorySpeedController::max(float d1, float d2){
    if (d1 >= d2){
        return d1;
    }else{
        return d2;
    }
}
